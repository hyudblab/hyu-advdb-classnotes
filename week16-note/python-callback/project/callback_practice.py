from typing import List, TypedDict, Callable


class Person(TypedDict):
    age: int
    name: str


data: List[Person] = [
    {"age": 10, "name": "ABC"},
    {"age": 30, "name": "BCD"},
    {"age": 40, "name": "CDF"},
    {"age": 70, "name": "DFG"},
]


def sum_age(data: List[Person], callback: Callable[[Person], bool]) -> int:
    total_age: int = 0
    for item in data:
        if callback(item):
            total_age += item.get("age", 0)
    return total_age


# sum_age == (List[Person], Any) -> int
# Callable[ [List[Person], Any], int ]

total = sum_age(data, lambda item: True if item.get("age") < 20 else False)
print(total)


total2 = sum_age(data, lambda item: True)
print(total2)
