from flask import Flask

app = Flask(__name__, static_folder="static", static_url_path="")
app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0


@app.route("/")
def index():
    return app.send_static_file("dist/index.html")


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
