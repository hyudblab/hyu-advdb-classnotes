import * as tf from '@tensorflow/tfjs'

async function main() {
  const model = await tf.loadLayersModel('/tfjs_model/model.json')
  const form = document.getElementById("imageFile")

  form.onsubmit = (ev: Event) => {
    ev.preventDefault()
    const target = ev.target as typeof ev.target & {
      image: { value: string, files: FileList }
    }
    const files = target.image.files
    if (files && files[0]) {
      const file = files[0]
      const reader = new FileReader()
      reader.onload = (ev: ProgressEvent<FileReader>) => {
        const image = new Image()
        image.onload = () => {
          const canvas = <HTMLCanvasElement>document.getElementById("canvas")
          const ctx = canvas.getContext('2d')
          ctx.drawImage(image, 0, 0)

          const imageData = ctx.getImageData(0, 0, 28, 28)
          const testImage = tf.browser.fromPixels(imageData, 1).squeeze()
          const reshapedImage = testImage.reshape([1, 28, 28])
          const result = <tf.Tensor<tf.Rank>>model.predict(reshapedImage)
          result.print()
          result.argMax(1).print()
        }

        // Load image from File
        const imageDataUrl = <string>ev.target.result
        image.src = imageDataUrl
      }

      // Load File
      reader.readAsDataURL(file)
    }
  }
}

window.onload = main
