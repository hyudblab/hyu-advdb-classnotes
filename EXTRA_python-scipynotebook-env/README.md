# Docker 실습 환경 활용하기

디렉토리 구조

```bash
python-scipynotebook-env
├── docker-compose.yml # Docker 컨테이너 설정 파일
├── .env # docker-compose.yml 에 적용될 환경변수
├── python # VScode Remote Container 에 사용될 설정 파일
├── project # 작업 폴더
└── README.md
```

## 실습환경 열기 - Docker

<code>docker-compose.yml</code> 파일과 <code>python</code> 폴더는 실습 환경 정보를 담고 있습니다.

이 두 폴더를 활용하여 여러분은 우리가 권장하는 실습 환경을 Docker 를 통해 각자의 PC에서 재현할 수 있습니다.

### 1. docker run 으로 실행

[과제0b](https://gitlab.com/hyudblab/postgres-starter/-/tree/master/homework_0b) 에서 실습한 내용처럼, docker 를 통해 현재 실습 에 필요한 환경을 가진 도커 이미지로 컨테이너를 생성할 수 있습니다. [docker CLI](https://gitlab.com/hyudblab/postgres-starter/-/tree/master/homework_0b#docker-%EC%82%AC%EC%9A%A9-%ED%99%98%EA%B2%BD%EC%97%90%EC%84%9C-%EC%8B%A4%ED%96%89) (<code>docker run</code> 명령어) 로 실행해서 Jupyter/scipy-notebook 이미지 가져와서 실행할 수 있습니다.

이 방법으로 환경을 설정하면

1. 최신 버전의 Python 을 기반으로 한 Jupyter/scipy-notebook 환경을 본인 PC 에 설치되어있는 환경을 오염시키지 않고 격리하여 실행시킬 수 있습니다.
2. 웹으로 접근하여 Jupyter notebook 을 사용합니다.
3. 접근 토큰은 매번 달라집니다. <code>docker logs</code> 로 확인하세요.


### 2. docker-compose up 으로 실행

<code>docker-compose up -d</code> 를 통해 이번 과제에 필요한 Jupyter 환경을 실행할 수 있습니다.

비밀번호는 미리 <code>1234</code> 로 설정되어 있습니다.

자신이 원하는 비밀번호를 미리 <code>.env</code> 파일의 <code>JUPYTER_PASSWORD</code> 에 입력하면 미리 설정할 수 있습니다.

비밀번호의 해쉬값을 생성하는 방법은 [공식 문서](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/common.html#notebook-options)를 참조하세요.

비밀번호 해쉬 생성 예제 - 컨테이너를 실행 후, <code>docker exec</code> 로 진입한 뒤, 명령줄에서 <code>python</code> 실행하여 python REPL 환경으로 진입한 하고나서
```python
from IPython.lib import passwd
passwd('2345')
```
실행 후 결과로 출력되는 해쉬값(sha1:~ 로 출력되는 값) 을 복사하여 .env 파일의 <code>JUPYTER_PASSWORD</code> 에 대체

예시:

![Cap 2020-09-18 16-10-51-406](https://user-images.githubusercontent.com/32762296/93567427-957d9700-f9c9-11ea-846a-1c23fb0b29a8.png)

jupyter 가 실행되는 포트는 8888 이 기본값입니다. docker-compose 파일의 ports 설정을 바꾸어 원하는 포트로 변경할 수 있습니다.

예시: 호스트 8888 포트를 58888 포트로 변경
```yml
...
ports:
    - "58888:8888"
...
```

자세한 내용은 docker-compose 사용법을 참조하세요.

### 3. VSCode Remote Container 환경

VSCode 의 Remote Container 확장 기능은 격리되어있는 Docker Container 의 환경을 사용하면서 Host PC 의 에디터에서 편집을 가능하게 해줍니다.

Docker 가 실행 환경을 동일하게 해준다면 Remote Container 을 통해 같은 개발환경도 통일할 수 있습니다.

자세한 내용은 [VSCode 설치](https://github.com/dreamwayjgs/hyu-database-class-2019/tree/master/0.%20Setup#vs-code-%EC%84%A4%EC%B9%98) 을 참조하여 설치하고 [Hello, world 예제](https://github.com/dreamwayjgs/hyu-database-class-2019/tree/master/1.%20hello-world)를 통해 VSCode + Docker 을 살펴보세요.

동영상 업로드 예정
