# Tensorflow Basic

텐서플로와 텐서 보드를 및 수업영상에서 사용한 fashion MNIST 코드를 활용하여 keras에서 제공하는 손글씨 데이터인 mnist에 대하여 **아래 조건과 같이 노드가 생성되도록** 코드를 colab에서 작성하고 결과물인 .ipynb 파일을 제출하세요.

## 문제 1
<code>model.summary()</code> 결과가 아래와 같을 때 <code>Param</code> 을 참조하여 모델의 shape을 결정하고 model을 구현하시오.  Layer의 이름은 달라도 상관없습니다. 

![Cap 2020-09-24 11-34-17-759](https://user-images.githubusercontent.com/32762296/94094225-eb36c100-fe59-11ea-8778-67e3c2beaa15.png)

## 문제 2
텐서 보드의 그래프를 참조하여 fit의 옵션을 조정하고 model layer를 결정하시오. (Layer의 이름은 달라도 상관없습니다)

![Cap 2020-09-24 11-40-56-700](https://user-images.githubusercontent.com/32762296/94094587-d60e6200-fe5a-11ea-8e84-429301077a39.png)

![조건2-2](https://user-images.githubusercontent.com/32762296/94094311-20431380-fe5a-11ea-8a5e-c48895457979.png)
