# Tensorflow Performance Comparison

6주차 과제

Colab 환경에서 CPU, GPU, TPU의 Training시 성능을 측정하여 제출하시오.
모델 생성은 자유입니다. (같은 모델을 각 환경에서 실행)
제출파일은 두 종류입니다.

- 아래 표를 작성하여 한글이나 워드 파일로 제출하시고
- 각자가 실험을 위해 작성한 Jupyter notebook 파일도 학번.ipynb 형태로 제출하시기 바랍니다. 


|CPU|GPU|TPU|
|------|---|---|
||||
