import os

with open(".docker-tensorflow-container.log") as f:
    name = f.readline()
    print("Trying to stop Container name:", name)
    result = os.system(f"docker stop {name} && docker rm {name}")    
    if result == 0:
        print("OK")
    else:
        print("Failed. Error code:", result)