import argparse
import os

password = "sha1:332560f18911:0ce7b5cbc623ff9f8d50b6bf00448c7b10346ab9"

parser = argparse.ArgumentParser()
parser.add_argument("-g", "--gpus", help="Number of gpus", default="all")
parser.add_argument("-p", "--port", help="Jupyter ports", default="48888")
parser.add_argument("-v", "--volume", help="Volumes", default="work")
parser.add_argument(
    "-n", "--name", help="Container name", default="docker-tensorflow-gpu"
)
parser.add_argument("-w", "--password", help="Password for jupyter", default=password)
args = parser.parse_args()

gpus = args.gpus
port = args.port
volume = args.volume
name = args.name
password = args.password
path = os.getcwd()
volume_path = os.path.join(os.getcwd(), volume)
print(volume_path)

matplot_path = "/usr/local/lib/python3.6/dist-packages/matplotlib/mpl-data/fonts/ttf"
korean_font_install_cmd = f"apt update && apt install -y wget && wget http://cdn.naver.com/naver/NanumFont/fontfiles/NanumFont_TTF_ALL.zip && unzip -o NanumFont_TTF_ALL.zip -d {matplot_path} && rm -rf .cache/matplotlib/* && rm -rf NanumFont_TTF_ALL.zip"

command = f'docker run -d --name {name} --gpus {gpus} -p {port}:8888 -v {volume_path}:/tf/work -e "JUPYTER_ENABLE_LAB=yes" tensorflow/tensorflow:2.3.1-gpu-jupyter bash -c "source /etc/bash.bashrc && {korean_font_install_cmd} && pip install jupyterlab && jupyter lab --notebook-dir=/tf --ip 0.0.0.0 --no-browser --allow-root --NotebookApp.password={password}"'

result = os.system(command)
if result == 0:
    print(f"Jupyter trying to run in container {name}, port {port}, using {gpus} gpus")
    with open(".docker-tensorflow-container.log", "w") as f:
        f.write(name)
        print("Container name saved", name)
else:
    print("Command failed. Code:", result)
