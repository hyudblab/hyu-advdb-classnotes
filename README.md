# 2020 고급데이터베이스및응용

이 곳은 수업에 관련된 코드, 노트, 과제 등을 사용하기 위한 저장소입니다.

과제0과 실습 환경 설정에 대한 내용은 [링크](https://gitlab.com/hyudblab/postgres-starter)를 참조하세요.

## 과제1 - 20200918
### 과제1-1 Python 기초
### 과제1-2 Numpy, Pandas, Matplotlib 기초


## Extracurricular

1. VScode Remote container + Docker 로 호스트와 격리된 실습 환경 구성하기 [동영상](https://youtu.be/QGgT6gTWMAY)
